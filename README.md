# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
$ git clone https://bitbucket.org/chrassy/stroboskop
```

Naloga 6.2.3:
```
$ git commit -a -m "Priprava potrebnih JavaScript knjižnic"
$ git push origin master

https://bitbucket.org/chrassy/stroboskop/commits/74b2c781742f4d2e9d3ba14a18ca445280eef5ee

```

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/chrassy/stroboskop/commits/7accb65e019f60e76f80e3448147d6c56b39df8a

Naloga 6.3.2:
https://bitbucket.org/chrassy/stroboskop/commits/45a770a07b84265d52583704fc026d2b314d11a5

Naloga 6.3.3:
https://bitbucket.org/chrassy/stroboskop/commits/694bae7af2eeb31326d67b99d2846e2c222179ba

Naloga 6.3.4:
https://bitbucket.org/chrassy/stroboskop/commits/fd1386a60a17068dc4f51871432d6fdbd565858f

Naloga 6.3.5:

```
$ git commit -m""
$ git checkout master
$ git merge izgled
$ git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/chrassy/stroboskop/commits/21fa1e7c35ffb3d19281cc9928b9b6790c28d587

Naloga 6.4.2:
https://bitbucket.org/chrassy/stroboskop/commits/71819d83946dc1384fe231509e482886119e9469

Naloga 6.4.3:
https://bitbucket.org/chrassy/stroboskop/commits/54aebfb5c5f262c3235c147291886c9dedfbd858

Naloga 6.4.4:
https://bitbucket.org/chrassy/stroboskop/commits/cf5255b9ffa48c4cb40590302a20860a119da566